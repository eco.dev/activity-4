console.log("Hello World!");

const info = {
	firstName: 'John',
	lastName: 'Smith',
	age: 30,
	hobbies: ['Biking', 'Mountain Climbing', 'Swimming'],
	address: {
		city: 'Lincoln',
		houseNumber: 32,
		state: 'Nebraska',
		street: 'Washington'
	},
	isMarried: false
};

console.log(
	`First Name: ${info.firstName}
Last Name: ${info.lastName}
Age: ${info.age}
Hobbies: ${info.hobbies}
Work Address:
City: ${info.address.city}
House Number: ${info.address.houseNumber}
State: ${info.address.state}
Street: ${info.address.street}`
);

function userInfo(info) {

	console.log(`John Smith is ${info.age} years of age.`);

	function userHobbies() {
		console.log(`His hobbies are: ${info.hobbies}`);
	}
	userHobbies(info.hobbies = ["eating", "sleeping", "bingewatching"]);

	console.log(
		`City: ${info.address.city}
House Number: ${info.address.houseNumber}
State: ${info.address.state}
Street: ${info.address.street}`
	);
}
userInfo(info);

function relStatus() {
	console.log('The value of isMarried is: ');
	console.log(info.isMarried);
}
relStatus();